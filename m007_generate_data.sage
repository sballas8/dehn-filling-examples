#from sage.all import *
#preparser(True)
import itertools
import time
import numpy as np
import pickle
import sys
import getopt
from sage.symbolic.expression_conversions import polynomial
start=time.time()
full_list=['smooth_equations','M1','M2','N1','N2']
if len(sys.argv)==1:
    deriv_list=full_list[:3]
    extra_eqns=False
else:
    deriv_list=[]
    argv=sys.argv[1:]
    opts,args=getopt.getopt(argv,'e')
    extra_eqns=True
    deriv_list=full_list
    #print(opts,args)
    #for opt, arg in opts:
    #    if arg in full_list:
    #        deriv_list+=[arg]
    #    else:
    #        #print()
    #        raise ValueError('{} needs to be an element of {}'.format(arg,full_list))
print(deriv_list)
print('derivatives to compute: {}'.format(deriv_list))

#returns numpy array whose components the order'th total derivatives of the components of f evaluated at basepoint
# basepoint should be a dictionary whose keys are the variables of f and whose values live in ring
def total_deriv_eval_np(f,order,variables,basepoint={},ring=ZZ,verbose=True):
    #var_list=variable_list(f)
    num_vars=len(variables)
    shape_list=[len(f)]+[num_vars]*order
    T=np.zeros(shape_list,dtype=object)
    total_derivs=len(f)*num_vars^order
    if verbose:
        print('there are {} partial derivatives to compute'.format(total_derivs))
    num_derivs=0
    pct_old=0
    f_parent=f[0].parent().fraction_field()
    phi=f_parent.hom([basepoint[gen] for gen in f_parent.gens()],f_parent.base_ring())
    for i in range(len(f)):
        #t=M.tensor((order,0),name='t')
        for multi_ix in itertools.product(range(num_vars),repeat=order):
            num_derivs+=1
            cpt=f[i].derivative([variables[ix] for ix in multi_ix])
            cpt2=phi(cpt)
            index_list=[i]+list(multi_ix)
            T[tuple(index_list)]=cpt2
            pct_new=num_derivs*100//total_derivs
            if pct_new %1==0 and pct_new>pct_old and verbose:
                print('done with {}% of partial derivatives'.format(pct_new),end='\r')
            pct_old=pct_new
    if verbose:
        print('completed all {} partial derivatives'.format(num_derivs))
    
    return T
print('making m007 directory')
try:
    os.mkdir('m007')
except FileExistsError:
    pass


print('creating variables')
variables=['a21','a23','a24','a31','a32','a34','a41','a42','a43','b21','b23','b24','b31','b32','b34',
          'c11','c12','c13','c14','c21','c22','c23','c24','c31','c32','c33','c34','c41','c42','c43','c44']
for ent in variables:
    var(ent)
    
    
    
#symbolic matrices, these multiply faster  

S=matrix([[-1,0,0,0],[1,1,0,0],[1,0,0,1],[1,0,1,0]])
Dc1=diagonal_matrix([c11,c12,c13,c14])
Dc2=diagonal_matrix([c21,c22,c23,c24])
Dc3=diagonal_matrix([c31,c32,c33,c34])
Dc4=diagonal_matrix([c41,c42,c43,c44])
A2=matrix([[0,0,1,a21],[0,0,0,-1],[0,1,0,a23],[1,0,0,a24]])
A3=matrix([[1,0,a31,0],[0,0,a32,1],[0,0,-1,0],[0,1,a34,0]])
A4=matrix([[1,0,a41,0],[0,1,a42,0],[0,0,a43,1],[0,0,-1,0]])
B2=matrix([[-1,b21,0,0],[1,-1-b21,0,0],[1,b23,1,0],[1,b24,0,1]])
B3=matrix([[b31,0,-1,0],[b32,0,1,1],[-1-b31,0,1,0],[b34,1,1,0]])

G1=A2*Dc1*(A4.inverse())
G2=S*Dc2*(A3.inverse())
G3=B3*Dc3*(A2.inverse())
G4=B2*Dc4*(A2.inverse())
G1i=G1.inverse()
G2i=G2.inverse()
G3i=G3.inverse()
G4i=G4.inverse()



print('variables created')

def char_poly_coeffs(A):
    c_list=[1]
    n=A.dimensions()[0]
    for ix in range(1,n+1):
        if ix==1:
            Ap=A
        else:
            Ap=A*(Ap+c_list[-1]*identity_matrix(n))
        new_c=-Ap.trace()/ix
        c_list+=[new_c.rational_simplify()]
    return c_list

def poly_eval(c_list,value):
    n=len(c_list)
    p_old=c_list[0]*value
    for ix in range(1,n):
        if ix==n-1:
            p_new=p_old+c_list[ix]
        else:
            p_new=value*(p_old+c_list[ix])
            p_old=p_new
    return p_new

def poly_deriv(c_list):
    n=len(c_list)
    d_list=[]
    for ix in range(n-1):
        ent=(n-1-ix)*c_list[ix]
        d_list+=[ent]
    return d_list

tm=G3*G2i
tn=G4*G2i*G2i*G4*G3i
print('computing characteritic polynomial of peripheral generators')
print('this may take a while')

cpm=char_poly_coeffs(tm)

if extra_eqns:
    cpn=char_poly_coeffs(tn)
print('done computing characteristic polynomials of peripheral generators')
#the number field containing the thurston paramters for the complete hyperbolic solution 
x=var('x')
K.<y>=NumberField(x^3+2*x+1)

my_string=''
for var in variables:
    my_string+=var +','
#my_string=my_string[:-1]
my_string=my_string[:-1]
func_ringK=K[my_string]
func_ringK.inject_variables(verbose=False)

#writes K as a vector space with basis 1,y,y^2 along with the maps to and from the vs to number field 
V,v2nf, nf2v=K.absolute_vector_space()
print('making basepoint')

bpt_K={
a21:v2nf(V((2,-1,1))),
a23:v2nf(V((2,-1,1))),
a24:v2nf(V((0,0,0))),
a31:v2nf(V((0,0,-1))),
a32:v2nf(V((1,1,1))),
a34:v2nf(V((1,-1,1))),
a41:v2nf(V((1,1,0))),
a42:v2nf(V((0,0,0))),
a43:v2nf(V((1,0,1))),
b21:v2nf(V((-2,1,-1))),
b23:v2nf(V((2,-1,1))),
b24:v2nf(V((4,-2,2))),
b31:v2nf(V((-1,-1,0))),
b32:v2nf(V((1,1,0))),
b34:v2nf(V((2,1,1))),
c11:v2nf(V((2,0,1))),
c12:v2nf(V((2,0,1))),
c13:v2nf(V((0,-1,0))),
c14:v2nf(V((0,-1,0))),
c21:v2nf(V((1,0,0))),
c22:v2nf(V((0,-1,0))),
c23:v2nf(V((1,0,0))),
c24:v2nf(V((2,0,1))),
c31:v2nf(V((1,0,0))),
c32:v2nf(V((1,0,0))),
c33:v2nf(V((1,0,0))),
c34:v2nf(V((1,0,0))),
c41:v2nf(V((0,-1,0))),
c42:v2nf(V((0,-1,0))),
c43:v2nf(V((2,0,1))),
c44:v2nf(V((2,0,1))),
}
vars_K=list(bpt_K.keys())

print('pickling basepoint')
dump_file=open('m007/bpt_K.p','wb')
pickle.dump(bpt_K,dump_file)
dump_file.close()
print('basepoint pickled')

#matrices involved in constructing the generators
s=matrix([[-1,0,0,0],[1,1,0,0],[1,0,0,1],[1,0,1,0]])
dc1=diagonal_matrix([c11,c12,c13,c14])
dc2=diagonal_matrix([c21,c22,c23,c24])
dc3=diagonal_matrix([c31,c32,c33,c34])
dc4=diagonal_matrix([c41,c42,c43,c44])
a2=matrix([[0,0,1,a21],[0,0,0,-1],[0,1,0,a23],[1,0,0,a24]])
a3=matrix([[1,0,a31,0],[0,0,a32,1],[0,0,-1,0],[0,1,a34,0]])
a4=matrix([[1,0,a41,0],[0,1,a42,0],[0,0,a43,1],[0,0,-1,0]])
b2=matrix([[-1,b21,0,0],[1,-1-b21,0,0],[1,b23,1,0],[1,b24,0,1]])
b3=matrix([[b31,0,-1,0],[b32,0,1,1],[-1-b31,0,1,0],[b34,1,1,0]])

#the generators
g1=a2*dc1*(a4.inverse())
g2=s*dc2*(a3.inverse())
g3=b3*dc3*(a2.inverse())
g4=b2*dc4*(a2.inverse())




#the relations
R1=g3*(g4.inverse())*g1-g2*(g4.inverse())*g2
R2=g3-g4*g1
R3=g2.inverse()-g3*g1

print('making smooth equations')
#the smooth equations 
sm_eqs=[dc1.det()-1,dc2.det()-1,dc3.det()-1,dc4.det()-1,
       R1[0,1],R1[0,2],R1[0,3],R1[1,0],R1[1,2],R1[1,3],R1[2,2],
       R1[2,3],R1[3,2],R2[0,0],R2[0,1],R2[0,3],R2[1,1],R2[1,3],  
       R2[2,1],R2[2,3],R2[3,1],R2[3,3],R3[0,0],R3[0,3],R3[1,0],
       R3[1,2],R3[1,3],R3[2,0] 
       ]

print('pickling smooth equations')

dump_file=open('m007/sm_eqs.p','wb')
pickle.dump(sm_eqs,dump_file)
dump_file.close()
print('smooth equations pickled')




print('computing type 1 equations')


cpmd=poly_deriv(cpm)
cpmd2=poly_deriv(cpmd)
cpmd_K=[]
cpmd2_K=[]

for ent in cpmd:
    if ent.parent()==ZZ:
        cpmd_K+=[ent]
    else:
        num=ent.numerator()
        den=ent.denominator()
        nump=polynomial(num,base_ring=K)
        denp=polynomial(den,base_ring=K)
        quot=func_ringK(nump)/func_ringK(denp)
        cpmd_K+=[quot]
for ent in cpmd2:
    if ent.parent()==ZZ:
        cpmd2_K+=[ent]
    else:
        num=ent.numerator()
        den=ent.denominator()
        nump=polynomial(num,base_ring=K)
        denp=polynomial(den,base_ring=K)
        quot=func_ringK(nump)/func_ringK(denp)
        cpmd2_K+=[quot]
        
if extra_eqns:

    cpnd=poly_deriv(cpn)
    cpnd2=poly_deriv(cpnd)
    cpnd_K=[]
    cpnd2_K=[]
    for ent in cpnd:
        if ent.parent()==ZZ:
            cpnd_K+=[ent]
        else:
            num=ent.numerator()
            den=ent.denominator()
            nump=polynomial(num,base_ring=K)
            denp=polynomial(den,base_ring=K)
            quot=func_ringK(nump)/func_ringK(denp)
            cpnd_K+=[quot]
    for ent in cpnd2:
        if ent.parent()==ZZ:
            cpnd2_K+=[ent]
        else:
            num=ent.numerator()
            den=ent.denominator()
            nump=polynomial(num,base_ring=K)
            denp=polynomial(den,base_ring=K)
            quot=func_ringK(nump)/func_ringK(denp)
            cpnd2_K+=[quot]


M1_K=poly_eval(cpmd_K,c33/c21)
M2_K=poly_eval(cpmd2_K,c33/c21)
dsm=total_deriv_eval_np(sm_eqs,1,vars_K,bpt_K,K,verbose=False)
dm=total_deriv_eval_np([M1_K,M2_K],1,vars_K,bpt_K,K,verbose=False)
jac=matrix(dsm)
vecm=vector(dm[1])
solm=jac.solve_left(vecm)
sm_eqs_vec=vector(sm_eqs)
M2_correction_term=sm_eqs_vec.dot_product(solm)
M2_corrected=M2_K-M2_correction_term
M2_K=M2_corrected
N1_K=None
N2_K=None

if extra_eqns:

    N1_K=poly_eval(cpnd_K,c41*c43/(c22*c24*c33))
    N2_K=poly_eval(cpnd2_K,c41*c43/(c22*c24*c33))



            

    dn=total_deriv_eval_np([N1_K,N2_K],1,vars_K,bpt_K,K,verbose=False)



    vecn=vector(dn[1])

    soln=jac.solve_left(vecn)



    N2_correction_term=sm_eqs_vec.dot_product(soln)


    N2_corrected=N2_K-N2_correction_term


    N2_K=N2_corrected

print('done building type 1 equations')

print('pickling type 1 equations')

dump_file=open('m007/type1_eqs.p','wb')
if extra_eqns:
    pickle.dump([M1_K,M2_K,N1_K,N2_K],dump_file)
    dump_file.close()
else:
    pickle.dump([M1_K,M2_K],dump_file)
    dump_file.close()
print('type 1 equations pickled')

deriv_dict={'smooth_equations':(sm_eqs,5,'sm'),'M1':([M1_K],6,'m1'),'M2':([M2_K],5,'m2'),'N1':([N1_K],6,'n1'),'N2':([N2_K],5,'n2')}
for deriv in deriv_list:
    (func,order,name)=deriv_dict[deriv]
    print('computing {} derivatives'.format(deriv))
    for ix in range(1,order):
        print('starting {}th derivative of {}'.format(ix,deriv))
        der=total_deriv_eval_np(func,ix,vars_K,bpt_K,K)
        print('done with {}th derivative of {}'.format(ix,deriv))
        print('pickling {}th derivative of {}'.format(ix,deriv))
        dump_file=open('m007/d{}{}.p'.format(ix,name),'wb')
        pickle.dump(der,dump_file)
        dump_file.close()
        print('done pickling {}th derivative of {}'.format(ix,deriv))
    print('done with derivatives of {}'.format(deriv))
end=time.time()

print('finished in {:.2f} minutes'.format((end-start)/60))             