# Unipotent Slope Examples #

This repository contains the code to verify computations in the paper (DEHN FILLING PAPER)

### Generating the data ###

To generate the data begin by running ```sage m007_generate_data.sage```. This will generate the necessary data to run the computations in ```
m007_computations.ipynb``` The data will be stored in a directory entitled ```m007```. By default, this will generate only the equations for one of the generators of the cusp. If you would like the data for the other generator run ```sage m007_generate_data.sage -e```. These computations will likely take several hours and the output will be a few gigabytes.

### The Calculations ###

Once you have generated the data, you can run the computations in the sage notebook ```m007_computations.ipynb```. This notebook will walk you through the computations necessary to show that the unipotent slope is non-constant in the direction of the type 1 cusp deformations. 

